# Gitlab CI/CD pipeline library

This project is the external project, using for sharing steps to execute CI/CD pipeline.

All projects need to link to this project for running the CI/CD.

## Structure

### Templates group structure
We will divide the structure of pipeline template into two groups:
- Configuration for all projects *(general cases)*, all of it will be stored in `templates` folder. That mean most of CI/CD cases will use functions in this folder
 
- Configuration of a specific case will be placed in the `gitlab namespace or gitlab group` of the running project
    > For exmaple: 
    > 
    >    The project `jayeson.portal.vodds_static` (https://gitlab.jayeson.com.sg/vodds/jayeson.portal.vodds_static) will have a different build and deploy configuration.
    > The pipeline functions for this project will store in `vodds` folder 


### Templates functional structure 
From two groups of templates above, each group will have multiple files. For each file will contains pipeline functions that support for a job in CI/CD pipeline.

> **Note**: Functions in files may link each other to have fully function, check `extends` for more information 

## Guideline to develop pipeline library project

Due to all CI/CD pipeline of integrated project will use branch `master` as the default version of pipeline Library, we should develop the pipeline in different branches.

And for the project using for testing CI/CD pipeline, we also need to change the **CI/CD configuration** in the `.gitlab.yaml` file

```yml
include:
  - project: 'devops/pipeline-library'
    ref: <development_branch_name>
    file: 'templates/main.yml'
```
